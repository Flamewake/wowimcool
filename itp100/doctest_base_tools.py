#Only works up to base 36
def to_base(num, base):
    if num == 0:
        return[0]
    digits = []
    while num:
        digits.append(int(num % base))
        num //= base
    for elem in digits:
        if elem >= 10:
            digits[digits.index(elem)] = chr(elem + 55)
    digits = ''.join([str(elem) for elem in digits])[::-1]
    return(digits)
print(to_base(65535, 64))

def to_base64num(num):
    digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    if num == 0:
        return "0"
    numstr = ""
    while num:
        numstr = digits[num % 64] + numstr
        num //= 64
    return f"{numstr}"

def to_base64(ys):
    sew = []
    sex = ""
    sis = []
    digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    test_str = str(ys)
    sib = ''.join(format(ord(i), 'b') for i in test_str)
    while sib:
        sis.append(sib[:7])
        sib = sib[7:]
    for i in sis:
        sis[sis.index(i)] = "0" + i
    siw = ''.join([str(i) for i in sis])
    ses = [(siw[i:i+6]) for i in range(0, len(siw), 6)]
    for i in ses:
        if len(i) < 6:
            ses[-1] = ses[-1] + "0"*(6-len(ses[-1]))
    [sew.append(int(binary, 2)) for binary in ses]
    for i in sew:
        sex = sex + digits[int(i)]
    if not (len(sex) % 4) == 0:
        sex = sex + ("=" * (4 - (len(sex) % 4)))
    return sex
print(to_base64("Man"))