def decode_base64(ys):
    # Base64 Digits
    digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    # Take string, remove padding
    test_str = str(ys).replace('=', "")
    # Split the string into characters
    strlist = [char for char in test_str]
    # Create the blank variables
    bins, o, fo = "", [], []
    # Go through the characters in the word.
    for s in strlist:
        # Find the decimal number for the digit
        loc = digits.find(s)
        # Turn the decimal number into a binary representation
        loc = format(loc, 'b')
        # This bit just adds the zeros to the beginning that got trimmed off
        if len(loc) < 6:
            loc = str(loc)
            loc = ("0" * (6 - len(loc))) + loc
        # Then you create a string of all the binary values
        bins = bins + loc
    # This splits the string of binary values into a list with each item being 8 digits (binary for ascii)
    while bins:
        o.append(bins[:8])
        bins = bins[8:]
    # This removes the leftover item that isn't a letter (The 0's added to the end by Base64)
    for s in o:
        if len(s) == 8:
            fo.append(s)
    # This turns all of the binary numbers into ascii characters and then makes them a string.
    fs = ''.join([chr(int(binary, 2)) for binary in fo])
    return fs
print(decode_base64("TWFu"))