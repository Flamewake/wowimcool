def base64decode(four_chars):
    """
      >>> base64decode('STOP')
      b'I3x8f'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    #Defines the alphabet to be used for indexing
    ints = [digits.index(ch) for ch in four_chars]  
    #list of the indexes from the string digits. Note ch's use
    b1 = (ints[0] << 2) | ((ints[1] & 48) >> 4)
    b2 = (ints[1] & 15) << 4 | ints[2] >> 2
    b3 = (ints[2] & 3) << 6 | ints[3]
    if not (len(b3) % 4) == 0:
        b3 = b3 + ("=" * (4 - (len(b3) % 4)))

    return bytes([b1, b2, b3])
print(base64decode('STOP'))