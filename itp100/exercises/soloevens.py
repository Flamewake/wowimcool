def only_evens(nums):
    """
    >>> only_evens([3, 8, 5, 4, 12, 7, 2])
    [8, 4, 12, 2]
    >>> my_nums = [4, 7, 19, 22, 42]
    >>> only_evens(my_nums)
    [4, 22, 42]
    >>> my_nums
    [4, 7, 19, 22, 42]
    """
    for num in nums:
        if num % 2 == 0:
            try:
                list1.append(num)
            except:
                list1 = [nums]
                
    return list1
    
def num_even_digits(n):
    """
    >>> num_even_digits(123456)
    3
    >>> num_even_digits(2468)
    4
    >>> num_even_digits(1357)
    0
    >>> num_even_digits(2)
    1
    >>> num_even_digits(20)
    2
    """
    nums = list(str(n))
    done = 0
    for num in nums:
        if int(num) % 2 == 0:
            done += 1
    return done

if __name__ == "__main__":
    import doctest
    doctest.testmod()
            
