from gasp import *

m_str = input("Enter the coordinated for your smiley face in the format, x y ")
m_list = m_str.split(" ")
x = int(m_list[0])
y = int(m_list[1])
x += 250
y += 250
begin_graphics()
Circle((x, y), 100)
Line((x, (y + 10)),((x - 10), (y - 10)))
Line(((x - 10), (y - 10)),((x + 10), (y - 10)))
Circle(((x - 15), (y + 10)), 10)
Circle(((x + 15), (y + 10)), 10)
Arc((x, y), 30, 225, 315)

update_when('key_pressed')

end_graphics()