from gasp import *

begin_graphics(width=640, height=640, title="aaa robots", background=color.GRAY)
gaming = 1
linev = 64
linec = 0
while linec < 9:
    Line((linev, 0), (linev, 640), color=color.BLACK)
    Line((0, linev), (640, linev), color=color.BLACK)
    linev += 64
    linec += 1
playinp = 0
playerx = 352
playery = 352
player = Circle((playerx, playery), 32, filled=True)
while gaming == 1:
    playinp = update_when('key_pressed')

    def move_p_left():
        move_by(player, -64, 0)
    
    def move_p_right():
        move_by(player, 64, 0)

    def move_p_up():
        move_by(player, 0, 64)

    def move_p_down():
        move_by(player, 0, -64)

    if playinp == 'a':
        move_p_left

    if playinp == 'w':
        move_p_up

    if playinp == 's':
        move_p_down

    if playinp == 'd':
        move_p_right


#update_when('key_pressed')
end_graphics()
