import tkinter as tk
from random import *

root = tk.Tk()
WIDTH = 1280
HEIGHT = 720
x1 = y1 = WIDTH / 2
canvas = tk.Canvas(root, width=WIDTH, height=HEIGHT)
c1 = canvas.create_rectangle(x1, y1, x1 - 60, y1 + 60, fill='red')
r1 = canvas.create_rectangle(x1 + 360, y1 - 360, x1 + 360, y1 - 360, fill='black')
left, right, up, down = -60,60,-60,60

for i in range(0, WIDTH, 60):
    canvas.create_line([(i, 0), (i, HEIGHT)], tag='grid_line')

for i in range(0, HEIGHT, 60):
    canvas.create_line([(0, i), (WIDTH, i)], tag='grid_line')

def robot_move():
    x = 0
    y = 0
    x_move = True
    y_move = True
    if canvas.coords(c1)[0] == canvas.coords(r1)[0] and canvas.coords(c1)[1] == canvas.coords(r1)[1]:
        canvas.delete(c1, r1, 'grid_line')
        canvas.create_text(x1, y1, font=("Purisa", 25), text="You Lost.")
    else:
        if canvas.coords(c1)[0] == canvas.coords(r1)[0]:
            x_move = False
        elif canvas.coords(c1)[1] == canvas.coords(r1)[1]:
            y_move = False
        # if x distance is greater than y distance, do x movement
        if abs(canvas.coords(c1)[0] - canvas.coords(r1)[0]) >= abs(canvas.coords(c1)[1] - canvas.coords(r1)[1]) and x_move:
            if canvas.coords(c1)[0] <= canvas.coords(r1)[0]:
                x = left
            elif canvas.coords(c1)[0] >= canvas.coords(r1)[0]:
                x = right
        elif y_move:
            if canvas.coords(c1)[1] >= canvas.coords(r1)[1]:
                y = down
            elif canvas.coords(c1)[1] <= canvas.coords(r1)[1]:
                y = up
            # if x distance is not greater than y movement, do y movement
        canvas.move(r1, x, y)
        


def keypress(event):
    robot_move()
    x = 0
    y = 0
    if event.char == "q":
        y = up
        x = left
    elif event.char == "w":
        y = up
    elif event.char == "e":
        y = up
        x = right
    elif event.char == "a":
        x = left
    elif event.char == "s":
        tpx = choice([left, right])
        tpx_severity = randint(1, 5)
        tpy = choice([down, up])
        tpy_severity = randint(1, 5)
        x = tpx * tpx_severity
        y = tpy * tpy_severity
    elif event.char == "d":
        x = right
    elif event.char == "z":
        y = down
        x = left
    elif event.char == "x":
        y = down
    elif event.char == "c":
        y = down
        x = right
    canvas.move(c1, x, y)


canvas.pack(fill=tk.BOTH, expand=True)
root.bind("<Key>", keypress)
root.mainloop()