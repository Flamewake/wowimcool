def itspure(x, y):
    pure = x + 2 * y
    return pure / (2 *x + y)

notpure = []

def itsnot(z):
    notpure.append(z)
    return notpure

print(itspure(2, 4))

print(itsnot('oh'))

