def countup_to_10():
    x = 0
    while x < 11:
        yield x
        x += 1

for x in countup_to_10():
    print(x)
