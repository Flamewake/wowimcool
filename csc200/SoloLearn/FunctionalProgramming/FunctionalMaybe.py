def inception(func, arg):
    return func(func(arg))

def sub_four(x):
    return x - 4

print(inception(sub_four, 20))
