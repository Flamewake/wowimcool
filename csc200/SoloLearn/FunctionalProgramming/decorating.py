def the_message():
    print("Hi people!")

the_message = decor(the_message)

@decor
def print_text():
    print("HI WORLD!")
